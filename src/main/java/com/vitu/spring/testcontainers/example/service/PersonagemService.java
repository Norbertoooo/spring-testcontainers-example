package com.vitu.spring.testcontainers.example.service;

import com.vitu.spring.testcontainers.example.domain.Personagem;
import com.vitu.spring.testcontainers.example.repository.PersonagemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonagemService {

    private final PersonagemRepository personagemRepository;

    public Personagem obterPersonagemPeloPseudonimo(String pseudonimo) throws Exception {
        log.info("Buscar personagem pelo pseudonimo: {}", pseudonimo);
        return personagemRepository.findByPseudonimo(pseudonimo).orElseThrow(IllegalArgumentException::new);
    }
}
