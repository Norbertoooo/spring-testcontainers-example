package com.vitu.spring.testcontainers.example.repository;

import com.vitu.spring.testcontainers.example.domain.Personagem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonagemRepository extends JpaRepository<Personagem, Long> {

    Optional<Personagem> findByPseudonimo(String pseudonimo);

}
