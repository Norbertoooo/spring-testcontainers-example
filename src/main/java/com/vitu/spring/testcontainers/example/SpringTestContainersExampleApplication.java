package com.vitu.spring.testcontainers.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTestContainersExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringTestContainersExampleApplication.class, args);
    }

}
