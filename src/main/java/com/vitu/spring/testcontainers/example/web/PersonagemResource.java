package com.vitu.spring.testcontainers.example.web;

import com.vitu.spring.testcontainers.example.domain.Personagem;
import com.vitu.spring.testcontainers.example.service.PersonagemService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/personagens")
public class PersonagemResource {

    private final PersonagemService personagemService;

    @GetMapping("/{pseudonimo}")
    public Personagem buscarPersonagemPeloPseudonimo(@PathVariable String pseudonimo) throws Exception {
        return personagemService.obterPersonagemPeloPseudonimo(pseudonimo);
    }

}
