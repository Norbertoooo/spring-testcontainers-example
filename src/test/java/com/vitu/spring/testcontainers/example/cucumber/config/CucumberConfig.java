package com.vitu.spring.testcontainers.example.cucumber.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class CucumberConfig {

    @Bean
    public void cucumberContextConfigurationLogger() {
        log.info("------------- Cucumber Context -------------");
    }

}
