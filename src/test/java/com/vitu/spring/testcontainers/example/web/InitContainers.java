package com.vitu.spring.testcontainers.example.web;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public abstract class InitContainers {

    @Container
    private final static MySQLContainer<?> mySQLContainer = new MySQLContainer<>("mysql")
            .withDatabaseName("banco_teste")
            .withUsername("username_teste")
            .withPassword("password_teste1!");

    @DynamicPropertySource
    static void databaseProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mySQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", mySQLContainer::getUsername);
        registry.add("spring.datasource.password", mySQLContainer::getPassword);
    }

    @BeforeEach
    void setUp() {
        mySQLContainer.start();
    }

    @AfterEach
    void tearDown() {
        mySQLContainer.stop();
    }

}
