package com.vitu.spring.testcontainers.example.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"}, tags = "@Integration", features = "src/test/resources/features")
public class CucumberRunnerTest {

}
