package com.vitu.spring.testcontainers.example.cucumber.steps;

import com.vitu.spring.testcontainers.example.domain.Personagem;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import lombok.extern.slf4j.Slf4j;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Slf4j
public class BuscarPersonagensStepdefs extends StepDefs {

    @Before
    public void limparBanco() {
        log.info("limpando banco");
        personagemRepository.deleteAll();
    }

    @Dado("que exista o seguinte personagem:")
    public void queExistaOSeguintePersonagem() {
        personagemRepository.save(Personagem.builder().pseudonimo("wolverine").build());
    }

    @Quando("for solicitada a busca de personagem pelo pseudonimo {string}")
    public void forSolicitadaABuscaDePersonagemPeloPseudonimo(String arg0) throws Exception {
        log.info(arg0);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/personagens" + "/" + arg0);

        mvcResult = mockMvc.perform(request).andDo(print()).andReturn();
    }

    @Então("deverá retorna o seguinte personagem")
    public void deveráRetornaOSeguintePersonagem() {
    }

}
