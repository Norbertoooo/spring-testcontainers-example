package com.vitu.spring.testcontainers.example;

import com.vitu.spring.testcontainers.example.web.InitContainers;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringTestContainersExampleApplicationTests extends InitContainers {

    @Test
    void contextLoads() {
    }

}
