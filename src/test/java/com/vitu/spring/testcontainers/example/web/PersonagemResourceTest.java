package com.vitu.spring.testcontainers.example.web;

import com.vitu.spring.testcontainers.example.domain.Personagem;
import com.vitu.spring.testcontainers.example.repository.PersonagemRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
class PersonagemResourceTest extends InitContainers {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    PersonagemRepository personagemRepository;

    @Test
    void buscarPersonagemPeloPseudonimo() throws Exception {

        personagemRepository.save(Personagem.builder().pseudonimo("wolverine").build());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/personagens/wolverine");

        boolean isWolverine = mockMvc.perform(requestBuilder)
                .andDo(print())
                .andReturn().getResponse().getContentAsString().contains("wolverine");

        assert isWolverine;
    }
}
