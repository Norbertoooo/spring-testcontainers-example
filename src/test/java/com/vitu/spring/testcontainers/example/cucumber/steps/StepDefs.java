package com.vitu.spring.testcontainers.example.cucumber.steps;

import com.vitu.spring.testcontainers.example.repository.PersonagemRepository;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
@CucumberContextConfiguration
public abstract class StepDefs {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected PersonagemRepository personagemRepository;

    protected static MvcResult mvcResult;

}
