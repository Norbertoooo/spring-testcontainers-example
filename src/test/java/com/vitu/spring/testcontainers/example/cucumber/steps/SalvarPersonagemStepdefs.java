package com.vitu.spring.testcontainers.example.cucumber.steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;

@Slf4j
public class SalvarPersonagemStepdefs extends StepDefs{

    @Dado("que não exista o seguinte personagem:")
    public void queNãoExistaOSeguintePersonagem() {
        personagemRepository.deleteAll();
    }

    @Quando("for solicitada a requisição para salvar o seguinte personagem:")
    public void forSolicitadaARequisiçãoParaSalvarOSeguintePersonagem() {
    }

    @Então("deverá retorna o código http {string}")
    public void deveráRetornaOCódigoHttp(String arg0) {
        Assertions.assertEquals(String.valueOf(mvcResult.getResponse().getStatus()), arg0);
    }

    @E("retornar as seguintes informações:")
    public void retornarAsSeguintesInformações() {
    }

}
