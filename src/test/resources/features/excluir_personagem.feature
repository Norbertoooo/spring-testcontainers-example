# language: pt

@Integration
Funcionalidade: Excluir personagem

  Cenário: Excluir personagem com sucesso
    Dado que exista o seguinte personagem:
    Quando for solicitada a requisição para excluir o personagem de pseudonimo "wolverine"
    Então deverá retorna o código http "201"
    E retornar as seguintes informações:
