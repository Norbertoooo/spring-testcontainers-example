# language: pt

@Integration
Funcionalidade: Buscar personagens

  Cenário: Buscar personagem com sucesso
    Dado que exista o seguinte personagem:
    Quando for solicitada a busca de personagem pelo pseudonimo "wolverine"
    Então deverá retorna o código http "200"
    Então deverá retorna o seguinte personagem
